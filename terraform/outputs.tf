output "external_ip_address_vm-pub" {
  value = yandex_compute_instance.bastion.network_interface.0.nat_ip_address
}

output "master_node_ip_address" {
  value = yandex_compute_instance.master-node[*].network_interface.0.ip_address
}

output "worker_node_ip_address" {
  value = yandex_compute_instance.worker-node[*].network_interface.0.ip_address
}
