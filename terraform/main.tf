provider "yandex" {
  #token    = local.token
  cloud_id  = local.cloud_id
  folder_id = local.folder_id
  zone      = var.zones[0]
}

/*-------------------1. Service account --------------------*/

// Create SA
resource "yandex_iam_service_account" "k8s_sa" {
  folder_id = local.folder_id
  name      = "k8s-admin"
}

// Grant permissions
resource "yandex_resourcemanager_folder_iam_member" "sa-edit1" {
  folder_id = local.folder_id
  role      = "compute.admin"
  member    = "serviceAccount:${yandex_iam_service_account.k8s_sa.id}"
}

resource "yandex_resourcemanager_folder_iam_member" "sa-edit2" {
  folder_id = local.folder_id
  role      = "vpc.admin"
  member    = "serviceAccount:${yandex_iam_service_account.k8s_sa.id}"
}

/*-------------------2. Bastion instance --------------------*/

resource "yandex_compute_instance" "bastion" {
  name        = "pub-instance"
  platform_id = "standard-v1"
  zone        = var.zones[0]

  resources {
    cores         = 2
    core_fraction = 20
    memory        = 1

  }

  scheduling_policy {
    # !прерываемая!
    preemptible = (terraform.workspace == "stage") ? true : false
  }

  boot_disk {
    initialize_params {
      image_id = "fd8mfc6omiki5govl68h" # Ubuntu 20.04 LTS
    }
  }

  network_interface {
    subnet_id = yandex_vpc_subnet.public.id
    nat       = true # Provide a public address, for instance, to access the internet over NAT
  }

  metadata = {
    user-data = "${file("meta.txt")}"
    ssh-keys = "ubuntu:${file("~/.ssh/id_rsa")}" # and private key only for bastion to
  }

}

/*------------------- Nodes instances --------------------*/

resource "yandex_compute_instance" "master-node" {
  count = (terraform.workspace == "stage") ? 1 : local.private_subnets
  name        = "master-node${count.index}"
  platform_id = "standard-v1"
  zone        = var.zones[count.index]

  resources {
    cores         = 2
    core_fraction = (terraform.workspace == "prod") ? 100 : 20
    memory        = 2
  }

  boot_disk {
    initialize_params {
      image_id = "fd8mfc6omiki5govl68h" # Ubuntu 20.04 LTS
      size = 50
    }
  }

  network_interface {
    subnet_id = yandex_vpc_subnet.private[count.index].id
    nat       = false
  }

  metadata = {
    user-data = "${file("meta.txt")}"
  }
}

resource "yandex_compute_instance" "worker-node" {
  count = local.private_subnets # create instance in each subnet
  name        = "worker-node${count.index}"
  platform_id = "standard-v1"
  zone        = var.zones[count.index]

  resources {
    cores         = 2
    core_fraction = (terraform.workspace == "prod") ? 100 : 5
    memory        = 2
  }

  boot_disk {
    initialize_params {
      image_id = "fd8mfc6omiki5govl68h" # Ubuntu 20.04 LTS
      size = 100
    }
  }

  network_interface {
    subnet_id = yandex_vpc_subnet.private[count.index].id
    nat       = false
  }

  metadata = {
    user-data = "${file("meta.txt")}"
  }
}

/*======================== AP Load Balancer ============================*/

resource "yandex_alb_target_group" "backend" {
  name      = "nodes-target-group"

  target {
    subnet_id = yandex_vpc_subnet.private[0].id
    ip_address   = yandex_compute_instance.worker-node[0].network_interface.0.ip_address
  }
  target {
    subnet_id = yandex_vpc_subnet.private[1].id
    ip_address   = yandex_compute_instance.worker-node[1].network_interface.0.ip_address
  }
  target {
    subnet_id = yandex_vpc_subnet.private[2].id
    ip_address   = yandex_compute_instance.worker-node[2].network_interface.0.ip_address
  }
}

resource "yandex_alb_load_balancer" "alb1" {
  name       = "app-load-balancer"
  network_id = yandex_vpc_network.vpc-1.id
  allocation_policy {
    location {
      zone_id   = var.zones[0]
      subnet_id = yandex_vpc_subnet.private[0].id
    }
    location {
      zone_id   = var.zones[1]
      subnet_id = yandex_vpc_subnet.private[1].id
    }
    location {
      zone_id   = var.zones[2]
      subnet_id = yandex_vpc_subnet.private[2].id
    }
  }
  listener {
    name = "app-listener"
    endpoint {
      address {
        external_ipv4_address {
          address = "51.250.29.98"
        }
      }
      ports = [80]
    }
    http {
      handler {
        http_router_id = yandex_alb_http_router.http-alb-router.id
      }
    }
  }
  listener {
    name = "app-listener"
    endpoint {
      address {
        external_ipv4_address {
          address = "51.250.29.98"
        }
      }
      ports = [443]
    }
    tls {
      default_handler {
        http_handler {
          http_router_id = yandex_alb_http_router.http-alb-router.id
        }
        certificate_ids  = var.tls_certs
      }
    }
  }
  listener {
    name = "grafana-listener"
    endpoint {
      address {
        external_ipv4_address {
          address = "51.250.29.98"
        }
      }
      ports = [3000]
    }
    tls {
      default_handler {
        http_handler {
          http_router_id = yandex_alb_http_router.grafana-alb-router.id
        }
        certificate_ids  = var.tls_certs
      }
    }
  }
}

/*------------------- App virtual host for Load Balancer ---------------------*/

resource "yandex_alb_http_router" "http-alb-router" {
  name      = "http-alb-router"
  labels = {
    tf-label    = "http"
    empty-label = ""
  }
}

resource "yandex_alb_backend_group" "app-group" {
  name      = "app-backend-group"

  http_backend {
    name = "app-backend"
    weight = 1
    port = 30070
    target_group_ids = ["${yandex_alb_target_group.backend.id}"]
    load_balancing_config {
    }
    healthcheck {
      timeout = "5s"
      interval = "5s"
      healthcheck_port = 30070
      http_healthcheck {
        path  = "/"
      }
    }
    http2 = "true"
  }
}

resource "yandex_alb_virtual_host" "app-virtual-host" {
  name      = "app-virtual-host"
  http_router_id = yandex_alb_http_router.http-alb-router.id
  authority = ["travel-pt.ru"]
  route {
    name = "app-route"
    http_route {
      http_route_action {
        backend_group_id = yandex_alb_backend_group.app-group.id
        timeout = "3s"
      }
    }
  }
}


/*------------------- Grafana virtual host for Load Balancer ---------------------*/

resource "yandex_alb_http_router" "grafana-alb-router" {
  name      = "grafana-alb-router"
  labels = {
    tf-label    = "http"
    empty-label = ""
  }
}

resource "yandex_alb_backend_group" "grafana-group" {
  name      = "grafana-backend-group"

  http_backend {
    name = "grafana-backend"
    weight = 1
    port = 30777
    target_group_ids = ["${yandex_alb_target_group.backend.id}"]
    load_balancing_config {
    }
    healthcheck {
      timeout = "5s"
      interval = "5s"
      healthcheck_port = 30777
      http_healthcheck {
        path  = "/"
      }
    }
    http2 = "true"
  }
}

resource "yandex_alb_virtual_host" "garafana-virtual-host" {
  name      = "grafana-virtual-host"
  http_router_id = yandex_alb_http_router.grafana-alb-router.id
  authority = ["monitor.travel-pt.ru"]
  route {
    name = "grafana-route"
    http_route {
      http_route_action {
        backend_group_id = yandex_alb_backend_group.grafana-group.id
        timeout = "3s"
      }
    }
  }
}


