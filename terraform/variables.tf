locals {
  # token   = "<OAuth>" # export YC_TOKEN="XXXXXXXXXXXXXXXXXXXXX-XXXXXXXXXXXX"
  folder_id = "b1gru9vkkhtb29co4kh5"
  cloud_id  = "b1gpm33d7a0h72fo204t"
  public_subnets = 1
  private_subnets = length(var.zones)
/*  [
    "${yandex_vpc_subnet.private[1].id}",
    "${yandex_vpc_subnet.private[2].id}",
    "${yandex_vpc_subnet.private[3].id}"
  ]*/
}

variable "zones" {
  type = list(string)
  default = [
    "ru-central1-a",
    "ru-central1-b",
    "ru-central1-c"
  ]
}

variable "private_v4_cidr_blocks" {
  type = list(list(string))
  default = [
    ["192.168.20.0/24"],
    ["192.168.40.0/24"],
    ["192.168.60.0/24"]
  ]
}

variable "public_v4_cidr_blocks" {
  type = list(list(string))
  default = [
    ["192.168.10.0/24"],
    ["192.168.30.0/24"],
    ["192.168.50.0/24"]
  ]
}

variable "tls_certs" {
  type = list(string)
  default = ["fpq9pr86qudqofeta920"]
}