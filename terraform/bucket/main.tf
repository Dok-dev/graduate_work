provider "yandex" {
  #token    = local.token
  cloud_id  = local.cloud_id
  folder_id = local.folder_id
  zone      = local.zone
}

/*------------------- Encrypted Bucket Object Storage --------------------*/

// Create Service Account
resource "yandex_iam_service_account" "sa" {
  folder_id = local.folder_id
  name      = "tf-editor-account"
}

// Grant permissions
resource "yandex_resourcemanager_folder_iam_member" "sa-edit1" {
  folder_id = local.folder_id
  role      = "storage.admin"
  member    = "serviceAccount:${yandex_iam_service_account.sa.id}"
}

/*
resource "yandex_resourcemanager_folder_iam_member" "sa-edit2" {
  folder_id = local.folder_id
  role      = "kms.editor" # Permission make ability to create, edit and delete any kms objects
  member    = "serviceAccount:${yandex_iam_service_account.sa.id}"
}

// Create KMS key for encrypting bucket
resource "yandex_kms_symmetric_key" "key-a" {
  name              = "symetric-key"
  description       = "encryption for bucket"
  default_algorithm = "AES_128"
  rotation_period   = "8760h" // equal to 1 year
}
*/

// Create Static Access Keys
resource "yandex_iam_service_account_static_access_key" "sa-static-key" {
  service_account_id = yandex_iam_service_account.sa.id
  description        = "static access key for object storage"
}


// Use keys to create bucket for backend
# documentation: https://registry.terraform.io/providers/yandex-cloud/yandex/latest/docs/resources/storage_bucket
resource "yandex_storage_bucket" "tf-bucket" {
  bucket     = "terraform-states"
  access_key = yandex_iam_service_account_static_access_key.sa-static-key.access_key
  secret_key = yandex_iam_service_account_static_access_key.sa-static-key.secret_key
  acl        = "private"

  versioning {
    # включение версионирования файла terraform.tfstate
    enabled = true
  }


/*  server_side_encryption_configuration {
    rule {
      apply_server_side_encryption_by_default {
        kms_master_key_id = yandex_kms_symmetric_key.key-a.id
        sse_algorithm     = "aws:kms"
      }
    }
  }*/

  #lifecycle {
  #  # запрет на удаление ресурса
  #  prevent_destroy = false
  #}
}
