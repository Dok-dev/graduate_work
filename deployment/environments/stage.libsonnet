{
  components: {

    "app": {
      "replicas": 3,
      "name": "web-app",
      "image": "0dok0/paintings",
      "ports": {
         "containerPort": 80
      },
      "service": {
         "port": 80
      }
    }
  }
}
