terraform {
  cloud {
    organization = "Dok"
    workspaces {
      name = "stage"
    }
  }
  required_providers {
    yandex = {
      source  = "yandex-cloud/yandex"
      version = "0.61.0"
    }
  }
}
