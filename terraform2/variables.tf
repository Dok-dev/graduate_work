variable "TFC_WORKSPACE_NAME" {
  type    = string
  default = ""
}

locals {
  # If backend is not Terraform Cloud, the value is ${terraform.workspace}
  # otherwise the value retrieved is that of the TFC_WORKSPACE_NAME with trimprefix
  # https://medium.com/@lichnguyen/using-terraform-workspace-with-terraform-cloud-993c31c1f8bc
  workspace = var.TFC_WORKSPACE_NAME != "" ? trimprefix("${var.TFC_WORKSPACE_NAME}", "ecs-") : "${terraform.workspace}"

  # token   = "<OAuth>" # export YC_TOKEN="XXXXXXXXXXXXXXXXXXXXX-XXXXXXXXXXXX"
  folder_id       = "b1gru9vkkhtb29co4kh5"
  cloud_id        = "b1gpm33d7a0h72fo204t"
  public_subnets  = 1
  private_subnets = length(var.zones)
  /*  [
    "${yandex_vpc_subnet.private[1].id}",
    "${yandex_vpc_subnet.private[2].id}",
    "${yandex_vpc_subnet.private[3].id}"
  ]*/
}

variable "user-data" {
  description = "Number of instances to provision."
  type        = string
  default     = ""
}

variable "zones" {
  type = list(string)
  default = [
    "ru-central1-a",
    "ru-central1-b",
    "ru-central1-c"
  ]
}

variable "private_v4_cidr_blocks" {
  type = list(list(string))
  default = [
    ["192.168.20.0/24"],
    ["192.168.40.0/24"],
    ["192.168.60.0/24"]
  ]
}

variable "public_v4_cidr_blocks" {
  type = list(list(string))
  default = [
    ["192.168.10.0/24"],
    ["192.168.30.0/24"],
    ["192.168.50.0/24"]
  ]
}

variable "tls_certs" {
  type = list(string)
  default = ["fpq9pr86qudqofeta920"]
}

# Конфиг для подключения к кластеру
locals {
  kubeconfig = <<KUBECONFIG
apiVersion: v1
clusters:
- cluster:
    server: ${yandex_kubernetes_cluster.rl_cluster_1.master[0].external_v4_endpoint}
    certificate-authority-data: ${base64encode(yandex_kubernetes_cluster.rl_cluster_1.master[0].cluster_ca_certificate)}
  name: kubernetes
contexts:
- context:
    cluster: kubernetes
    user: ${yandex_iam_service_account.k8s_sa.name}
  name: ycmk8s
current-context: ycmk8s
users:
- name: ${yandex_iam_service_account.k8s_sa.name}
  user:
    exec:
      command: yc
      apiVersion: client.authentication.k8s.io/v1beta1
      interactiveMode: Never
      args:
      - k8s
      - create-token
KUBECONFIG
}