provider "yandex" {
  #token    = local.token
  cloud_id  = local.cloud_id
  folder_id = local.folder_id
  zone      = var.zones[0]
}

/*-------------------1. Service account --------------------*/

// Create SA
resource "yandex_iam_service_account" "k8s_sa" {
  folder_id = local.folder_id
  name      = "k8s-admin"
}

// Grant permissions
resource "yandex_resourcemanager_folder_iam_member" "sa-edit1" {
  folder_id   = local.folder_id
  role        = "k8s.editor" # Permission make ability to create, edit and delete k8s objects
  member      = "serviceAccount:${yandex_iam_service_account.k8s_sa.id}"
  sleep_after = 30
}
resource "yandex_resourcemanager_folder_iam_member" "sa-edit2" {
  folder_id = local.folder_id
  role      = "kms.editor" # Permission make ability to create, edit and delete kms objects
  member    = "serviceAccount:${yandex_iam_service_account.k8s_sa.id}"
}
resource "yandex_resourcemanager_folder_iam_member" "sa-edit3" {
  folder_id = local.folder_id
  # role        = "k8s.cluster-api.editor" // разрешение почему-то не работает
  role        = "editor"
  member      = "serviceAccount:${yandex_iam_service_account.k8s_sa.id}"
  sleep_after = 30
}

/*-------------------2. KMS --------------------*/

// Create KMS key
/*resource "yandex_kms_symmetric_key" "key-a" {
  name              = "symetric-key"
  description       = "encryption for bucket"
  default_algorithm = "AES_128"
  rotation_period   = "8760h" // equal to 1 year
  depends_on        = [yandex_resourcemanager_folder_iam_member.sa-edit2]
}*/

/*-------------------3. Nodes instances --------------------*/

# Создадим группу мастеров (control plane nodes)
resource "yandex_kubernetes_cluster" "rl_cluster_1" {
  name        = "kubernetes-cluster1"
  description = "k8s cluster"

  network_id = yandex_vpc_network.vpc-1.id

  master {
    version   = "1.21"
    public_ip = true

    zonal {
      zone      = var.zones[0]
      subnet_id = yandex_vpc_subnet.private[0].id
    }

    maintenance_policy {
      auto_upgrade = true

      maintenance_window {
        day        = "monday"
        start_time = "01:00"
        duration   = "3h"
      }

      maintenance_window {
        day        = "friday"
        start_time = "02:00"
        duration   = "4h30m"
      }
    }
  }

  service_account_id      = yandex_iam_service_account.k8s_sa.id
  node_service_account_id = yandex_iam_service_account.k8s_sa.id

  /*  labels = {
    my_label = "my_label1"
  }*/

  //Ключ Yandex KMS для шифрования важной информации, такой как пароли, OAuth-токены и SSH-ключи (так называемые secrets).
  /*  kms_provider {
    key_id = yandex_kms_symmetric_key.key-a.id
  }*/

  release_channel = "STABLE"

  depends_on = [
    yandex_resourcemanager_folder_iam_member.sa-edit1,
    yandex_resourcemanager_folder_iam_member.sa-edit3
  ]
}


# Создадим worker nodes
resource "yandex_kubernetes_node_group" "node_group1" {
  cluster_id  = yandex_kubernetes_cluster.rl_cluster_1.id
  name        = "node-group1"
  description = "worker nodes"
  version     = "1.21"

  labels = {
    "key" = "value"
  }

  instance_template {
    platform_id = "standard-v1"

    network_interface {
      nat = false # Provide a public address, for instance, to access the internet over NAT
      # В группе с автоматическим масштабированием возможна только одна зона доступности
      subnet_ids = [yandex_vpc_subnet.private[0].id, yandex_vpc_subnet.private[1].id, yandex_vpc_subnet.private[2].id]
    }

    resources {
      memory        = 2
      cores         = 2
      core_fraction = 5 # % CPU
    }

    boot_disk {
      type = "network-hdd"
      size = 100
    }

    metadata = {
      # user-data = "${file("meta.txt")}"
      user-data = var.user-data
    }

  }

  scale_policy {
    # !!!  В группе с автоматическим масштабированием возможна только одна зона доступности !!!
    /*    auto_scale {
      initial = 0
      max     = 0
      min     = 0
    }*/
    fixed_scale {
      size = 3
    }
  }

  allocation_policy {
    location {
      zone = var.zones[0]
    }
    location {
      zone = var.zones[1]
    }
    location {
      zone = var.zones[2]
    }
  }

  maintenance_policy {
    auto_upgrade = true
    auto_repair  = true

    maintenance_window {
      day        = "monday"
      start_time = "5:00"
      duration   = "3h"
    }

    maintenance_window {
      day        = "friday"
      start_time = "1:00"
      duration   = "4h30m"
    }
  }

  depends_on = [
    yandex_resourcemanager_folder_iam_member.sa-edit1,
    yandex_resourcemanager_folder_iam_member.sa-edit3
  ]
}

/*======================== AP Load Balancer ============================*/


resource "yandex_alb_load_balancer" "alb1" {
  name       = "app-load-balancer"
  network_id = yandex_vpc_network.vpc-1.id
  allocation_policy {
    location {
      zone_id   = var.zones[0]
      subnet_id = yandex_vpc_subnet.private[0].id
    }
    location {
      zone_id   = var.zones[1]
      subnet_id = yandex_vpc_subnet.private[1].id
    }
    location {
      zone_id   = var.zones[2]
      subnet_id = yandex_vpc_subnet.private[2].id
    }
  }
  listener {
    name = "app-listener"
    endpoint {
      address {
        external_ipv4_address {
          address = "51.250.29.98"
        }
      }
      ports = [80]
    }
    http {
      handler {
        http_router_id = yandex_alb_http_router.http-alb-router.id
      }
    }
  }
  listener {
    name = "app-listener"
    endpoint {
      address {
        external_ipv4_address {
          address = "51.250.29.98"
        }
      }
      ports = [443]
    }
    tls {
      default_handler {
        http_handler {
          http_router_id = yandex_alb_http_router.http-alb-router.id
        }
        certificate_ids  = var.tls_certs
      }
    }
  }
  listener {
    name = "grafana-listener"
    endpoint {
      address {
        external_ipv4_address {
          address = "51.250.29.98"
        }
      }
      ports = [3000]
    }
    tls {
      default_handler {
        http_handler {
          http_router_id = yandex_alb_http_router.grafana-alb-router.id
        }
        certificate_ids  = var.tls_certs
      }
    }
  }
}

resource "yandex_alb_http_router" "http-alb-router" {
  name = "http-alb-router"
  labels = {
    tf-label    = "http"
    empty-label = ""
  }
}

resource "yandex_alb_http_router" "grafana-alb-router" {
  name = "http-alb-router"
  labels = {
    tf-label    = "http"
    empty-label = ""
  }
}

# Далее надо накликивать таргеты в web-интерфейсе т.к. Яндекс на возвращает необходимые данные из yandex_kubernetes_node_group в Data Sorces.